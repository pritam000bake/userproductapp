﻿using ProductWebApplication.MVC.Exceptions;
using ProductWebApplication.MVC.Models;
using ProductWebApplication.MVC.Repository;

namespace ProductWebApplication.MVC.Services
{
    public class ProductService : IProductService
    {
        readonly IProductRepository _productRepository;
        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public int AddProduct(Product product)
        {
            Product? _product = _productRepository.GetProductByName(product.Name);
            if(_product == null)
            {
                return _productRepository.AddProduct(product);
            }
            else
            {
                throw new ProductAlreadyExistException($"{product.Name} Product Already Exist !!");
            }
        }
        public int DeleteProduct(int id)
        {
            Product? productDetails =_productRepository.GetProductById(id);
            if (productDetails != null)
            {
                return _productRepository.DeleteProduct(productDetails);
            }
            else
            {
                throw new ProductNotFoundException("This Product Not Found");
            } 
        }
        public List<Product> GetProduct()
        {
            return _productRepository.GetProduct();
        }
        public Product? GetProductById(int id)
        {
            Product? productDetails = _productRepository.GetProductById(id);
            if( productDetails != null)
            {
                return productDetails;
            }
            else
            {
                throw new ProductNotFoundException("This Product Not Found");
            }
        }
        public int UpdateProduct(Product product)
        {
           
            return _productRepository.UpdateProduct(product);
        }
    }
}
