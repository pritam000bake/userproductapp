﻿using ProductWebApplication.MVC.Models;

namespace ProductWebApplication.MVC.Services
{
    public interface IProductService
    {
        List<Product> GetProduct();
        int AddProduct(Product product);
        int DeleteProduct(int id);
        int UpdateProduct(Product product);
        public Product? GetProductById(int id);
    }
}
