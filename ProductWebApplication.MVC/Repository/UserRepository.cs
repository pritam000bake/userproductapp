﻿using ProductWebApplication.MVC.Context;
using ProductWebApplication.MVC.Models;

namespace ProductWebApplication.MVC.Repository
{
    public class UserRepository:IUserRepository
    {
        ProductDbContext _productDbContext;
        public UserRepository(ProductDbContext productDbContext)
        {
            _productDbContext = productDbContext;
        }

        public bool AddUser(User user)
        {
            _productDbContext.Users.Add(user);
            return _productDbContext.SaveChanges() == 1 ? true : false;
        }

        public List<User> GetAllUsers()
        {
            return _productDbContext.Users.ToList();
        }

        public User? GetUserByName(string? name)
        {
            return _productDbContext.Users.Where(q => q.Name == name).FirstOrDefault();
        }

        public User Login(string? name, string? password)
        {
            return _productDbContext.Users.Where(q => q.Name == name && q.Password == password).FirstOrDefault();
        }
    }
}
